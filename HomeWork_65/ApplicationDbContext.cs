﻿using HomeWork_65.Entities;
using HomeWork_65.EntitiesConfiguration;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork_65
{
    public class ApplicationDbContext : IdentityDbContext<User,Role,int>
    {
        private readonly IEntityConfigurationsContainer _entityConfigurationsContainer;

        public DbSet<Dish> Dishes { get; set; }
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<Basket> Baskets { get; set; }


        public ApplicationDbContext(
            DbContextOptions options,
            IEntityConfigurationsContainer entityConfigurationsContainer) : base(options)
        {
            _entityConfigurationsContainer = entityConfigurationsContainer;

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity(_entityConfigurationsContainer.RestaurantConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.DishConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.BasketConfiguration.ProvideConfigurationAction());
        }
    }
}
