﻿using HomeWork_65.Repositories.Contracts;
using System;

namespace HomeWork_65.Repositories
{
    public class UnitOfWork : IDisposable
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed;

        public IDishRepository Dish { get; set; }
        public IRestaurantRepository Restaurant { get; set; }
        public IBasketRepository Basket { get; set; }


        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            Dish = new DishRepository(context);
            Restaurant = new RestaurantRepository(context);
            Basket = new BasketRepository(context);
        }

        #region Disposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _context.Dispose();

            _disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
        #endregion
    }
}
