﻿using HomeWork_65.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork_65.Repositories.Contracts
{
    public interface IDishRepository : IRepository<Dish>
    {
        IEnumerable<Dish> GetAllWithRestaurant();
        Dish GetByIdWithAll(int id);
    }
}
