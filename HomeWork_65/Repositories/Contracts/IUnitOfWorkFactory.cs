﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork_65.Repositories.Contracts
{
    public interface IUnitOfWorkFactory
    {
        UnitOfWork Create();
    }
}
