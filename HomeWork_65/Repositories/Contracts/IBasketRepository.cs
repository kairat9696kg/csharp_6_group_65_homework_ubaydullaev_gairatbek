﻿using HomeWork_65.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork_65.Repositories.Contracts
{
    public interface IBasketRepository : IRepository<Basket>
    {
        IEnumerable<Basket> GetAllByUserId(int id);
        Basket GetBasketByDishId(int id, int userId);
    }
}
