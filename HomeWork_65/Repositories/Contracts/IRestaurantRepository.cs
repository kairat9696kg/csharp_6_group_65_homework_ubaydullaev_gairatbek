﻿using HomeWork_65.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork_65.Repositories.Contracts
{
    public interface IRestaurantRepository : IRepository<Restaurant>
    {
        IEnumerable<Restaurant> GetAllWithDish();
        Restaurant GetByIdWithDishes(int id);
    }
}
