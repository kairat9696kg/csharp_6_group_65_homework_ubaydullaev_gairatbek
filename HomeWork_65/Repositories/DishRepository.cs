﻿using HomeWork_65.Entities;
using HomeWork_65.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HomeWork_65.Repositories
{
    public class DishRepository : Repository<Dish>, IDishRepository
    {
        public DishRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Dishes;
        }

        public IEnumerable<Dish> GetAllWithRestaurant()
        {
            return entities.Include(e => e.Restaurant);
        }

        public Dish GetByIdWithAll(int id)
        {
            return entities
                .Include(e => e.Restaurant)
                .FirstOrDefault(e => e.Id == id);
        }
    }
}
