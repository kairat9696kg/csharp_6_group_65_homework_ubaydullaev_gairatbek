﻿using HomeWork_65.Entities;
using HomeWork_65.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace HomeWork_65.Repositories
{
    public class RestaurantRepository : Repository<Restaurant>, IRestaurantRepository
    {
        public RestaurantRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Restaurants;
        }

        public IEnumerable<Restaurant> GetAllWithDish()
        {
            return entities
                .Include(e => e.Dishes);
        }

        public Restaurant GetByIdWithDishes(int id)
        {
            return entities
                .Include(e => e.Dishes)
                .FirstOrDefault(e => e.Id == id);
        }
    }
}
