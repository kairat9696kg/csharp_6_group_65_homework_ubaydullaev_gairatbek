﻿using HomeWork_65.Entities;
using HomeWork_65.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace HomeWork_65.Repositories
{
    public class BasketRepository : Repository<Basket>, IBasketRepository
    {
        public BasketRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Baskets;
        }

        public IEnumerable<Basket> GetAllByUserId(int id)
        {
            return entities
                .Include(e => e.Restaurant)
                .Include(e => e.User)
                .Include(e => e.Dish)
                .Where(e => e.UserId == id);
        }

        public Basket GetBasketByDishId(int id,int userId)
        {
            return entities
                .Where(e => e.DishId == id)
                .FirstOrDefault(e => e.UserId == userId);
        }
    }
}
