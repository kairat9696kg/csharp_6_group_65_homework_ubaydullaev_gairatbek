﻿using System.Collections;
using System.Collections.Generic;

namespace HomeWork_65.Entities
{
    public class Dish : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public int RestaurantId { get; set; }
        public Restaurant Restaurant { get; set; }
        public ICollection<Basket> Baskets { get; set; }
    }
}
