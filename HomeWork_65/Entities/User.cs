﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork_65.Entities
{
    public class User : IdentityUser<int>, IEntity
    {      
       public ICollection<Basket> Baskets { get; set; }
    }
}
