﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork_65.Entities
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
