﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork_65.Entities
{
    public class Basket : IEntity
    {
        public int Id { get; set; }
        public decimal Sum { get; set; }
        public int DishCount { get; set; }
        public int DishId { get; set; }
        public Dish Dish { get; set; }
        public int Restaurantid { get; set; }
        public Restaurant Restaurant { get; set; }
        public int UserId { get; set; }
        public User User { get;set; }

    }
}
