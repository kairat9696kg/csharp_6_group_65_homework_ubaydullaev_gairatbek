﻿using HomeWork_65.Entities;
using HomeWork_65.EntitiesConfiguration.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork_65.EntitiesConfiguration
{
    public class EntityConfigurationsContainer : IEntityConfigurationsContainer
    {
        public IEntityConfiguration<Restaurant> RestaurantConfiguration { get; }
        public IEntityConfiguration<Dish> DishConfiguration { get; }
        public IEntityConfiguration<Basket> BasketConfiguration { get; }
        public EntityConfigurationsContainer()
        {
            RestaurantConfiguration = new RestaurantConfiguration();
            DishConfiguration = new DishConfiguration();
            BasketConfiguration = new BasketConfiguration();
        }
    }
}
