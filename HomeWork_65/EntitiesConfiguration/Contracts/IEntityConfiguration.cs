﻿using HomeWork_65.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace HomeWork_65.EntitiesConfiguration.Contracts
{
    public interface IEntityConfiguration<T> where T : class, IEntity
    {
        Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
    }
}