﻿using HomeWork_65.Entities;
using HomeWork_65.EntitiesConfiguration.Contracts;

namespace HomeWork_65.EntitiesConfiguration
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<Restaurant> RestaurantConfiguration { get; }
        IEntityConfiguration<Dish> DishConfiguration { get; }
        IEntityConfiguration<Basket> BasketConfiguration  { get; }
    }
}