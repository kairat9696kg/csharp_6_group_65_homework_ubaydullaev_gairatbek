﻿using HomeWork_65.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HomeWork_65.EntitiesConfiguration
{
    public class DishConfiguration : BaseEntityConfiguration<Dish>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Dish> builder)
        {
            builder
                .Property(b => b.Name)
                .HasMaxLength(100)
                .IsRequired();
            builder
                .Property(b => b.Description)
                .HasMaxLength(350)
                .IsRequired();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Dish> builder)
        {
            builder
                .HasOne(p => p.Restaurant)
                .WithMany(p => p.Dishes)
                .OnDelete(DeleteBehavior.Restrict);
            builder
                .HasMany(p => p.Baskets)
                .WithOne(p => p.Dish)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
