﻿using HomeWork_65.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork_65.EntitiesConfiguration
{
    public class RestaurantConfiguration : BaseEntityConfiguration<Restaurant>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Restaurant> builder)
        {
            builder
                .Property(b => b.Name)
                .HasMaxLength(100)
                .IsRequired();
            builder
                .Property(b => b.Description)
                .HasMaxLength(350)
                .IsRequired();            
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Restaurant> builder)
        {
            builder
                .HasMany(p => p.Dishes)
                .WithOne(p => p.Restaurant)
                .OnDelete(DeleteBehavior.Restrict);
            builder
                .HasMany(p => p.Baskets)
                .WithOne(p => p.Restaurant)
                .OnDelete(DeleteBehavior.Restrict);

        }
    }
}
