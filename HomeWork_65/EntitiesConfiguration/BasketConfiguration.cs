﻿using HomeWork_65.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork_65.EntitiesConfiguration
{
    public class BasketConfiguration : BaseEntityConfiguration<Basket>
    {

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Basket> builder)
        {
            builder
                .HasOne(p => p.Restaurant)
                .WithMany(p => p.Baskets)
                .OnDelete(DeleteBehavior.Restrict);

            builder
               .HasOne(p => p.Dish)
               .WithMany(p => p.Baskets)
               .OnDelete(DeleteBehavior.Restrict);
            
            builder
               .HasOne(p => p.User)
               .WithMany(p => p.Baskets)
               .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
