﻿namespace HomeWork_65
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext Create();
    }
}