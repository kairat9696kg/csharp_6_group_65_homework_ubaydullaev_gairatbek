﻿using AutoMapper;
using Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Models;
using Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Services.Contracts;
using HomeWork_65.Entities;
using HomeWork_65.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Services
{
    public class BasketService : IBasketService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public BasketService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public void AddToBasket(int id, int userId)
        {
            using(var unitOfWork = _unitOfWorkFactory.Create())
            {
                var dish = unitOfWork.Dish.GetByIdWithAll(id);
                var basket = unitOfWork.Basket.GetBasketByDishId(id,userId);
                if(basket == null)
                {
                    basket = CreaetBasket(dish, userId);
                    unitOfWork.Basket.Create(basket);
                }
                else
                {
                    basket = UpdateBasket(basket, dish, userId);
                    unitOfWork.Basket.Update(basket);
                }               
            }
        }


        
        public List<BasketModel> ShowBaskets(int id)
        {
            using(var unitOfWork = _unitOfWorkFactory.Create())
            {
                List<Basket> baskets = unitOfWork.Basket.GetAllByUserId(id).ToList();
                if (baskets == null)
                    throw new ArgumentNullException(nameof(baskets));
                List<BasketModel> basketModels = Mapper.Map<List<BasketModel>>(baskets);
                return basketModels;
            }
        }

        private Basket UpdateBasket(Basket basket, Dish dish, int userId)
        {
            basket.DishCount++;
            basket.Sum = basket.Sum + dish.Price;
            return basket;
        }
        private Basket CreaetBasket(Dish dish, int userId)
        {
            var basket = new Basket()
            {
                Restaurantid = dish.RestaurantId,
                UserId = userId,
                Sum = dish.Price,
                DishCount = 1,
                DishId = dish.Id
            };
            return basket;
        }


    }
}
