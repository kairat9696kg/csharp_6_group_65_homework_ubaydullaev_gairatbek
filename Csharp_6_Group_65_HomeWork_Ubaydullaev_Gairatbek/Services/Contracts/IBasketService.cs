﻿using Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Services.Contracts
{
    public interface IBasketService
    {
        List<BasketModel> ShowBaskets(int id);
        void AddToBasket(int id, int currentUser);
    }
}
