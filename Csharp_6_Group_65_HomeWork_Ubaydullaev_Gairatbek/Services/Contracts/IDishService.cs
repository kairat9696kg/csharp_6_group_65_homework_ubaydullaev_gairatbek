﻿using Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Models;
using HomeWork_65.Entities;
using System.Collections.Generic;

namespace Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Services.Contracts
{
    public interface IDishService
    {
        IEnumerable<DishModel> GetAllDishes();
        DishCreateModel GetDishCreateModel();
        void Create(DishCreateModel model);
    }
}