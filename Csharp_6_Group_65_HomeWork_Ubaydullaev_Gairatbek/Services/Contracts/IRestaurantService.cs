﻿using Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Models;
using System.Collections.Generic;

namespace Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Services.Contracts
{
    public interface IRestaurantService
    {
        IEnumerable<RestaurantModel> GetAllRestaurant();
        void Create(RestaurantCreateModel model);
        RestaurantInfoModel GetRestaurantById(int id);
    }
}