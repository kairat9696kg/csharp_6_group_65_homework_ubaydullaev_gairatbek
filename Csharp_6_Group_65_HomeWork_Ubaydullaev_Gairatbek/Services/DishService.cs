﻿using AutoMapper;
using Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Models;
using Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Services.Contracts;
using HomeWork_65.Entities;
using HomeWork_65.Repositories.Contracts;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Services
{
    public class DishService : IDishService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public DishService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public void Create(DishCreateModel model)
        {
            using(var unitOfWork = _unitOfWorkFactory.Create())
            {
                Dish dish = Mapper.Map<Dish>(model);
                unitOfWork.Dish.Create(dish);
            }
        }

        public IEnumerable<DishModel> GetAllDishes()
        {
            using(var unitOfWork = _unitOfWorkFactory.Create())
            {
                var Dishes = unitOfWork.Dish.GetAllWithRestaurant();

                List<DishModel> dishModels = Mapper.Map<List<DishModel>>(Dishes);

                return dishModels;
            }
        }

        public DishCreateModel GetDishCreateModel()
        {
            return new DishCreateModel()
            {
                RestorantsSelect = GetRestaurantSelecList()
            };

        }

        public SelectList GetRestaurantSelecList()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var restaurants = unitOfWork.Restaurant.GetAll().ToList();
                return new SelectList(restaurants, nameof(Restaurant.Id), nameof(Restaurant.Name));
            }
        }
    }
}
