﻿using AutoMapper;
using Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Models;
using Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Services.Contracts;
using HomeWork_65.Entities;
using HomeWork_65.Repositories.Contracts;
using System;
using System.Collections.Generic;

namespace Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Services
{
    public class RestaurantService : IRestaurantService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public RestaurantService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public void Create(RestaurantCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var restaurant = Mapper.Map<Restaurant>(model);

                unitOfWork.Restaurant.Create(restaurant);
            }
        }

        public IEnumerable<RestaurantModel> GetAllRestaurant()
        {
            using(var unitOfWork = _unitOfWorkFactory.Create())
            {
                var Restaurants = unitOfWork.Restaurant.GetAll();

                List<RestaurantModel> restaurantModels = Mapper.Map<List<RestaurantModel>>(Restaurants);


                return restaurantModels;
            }
        }

        public RestaurantInfoModel GetRestaurantById(int id)
        {
            using(var unitOfWork = _unitOfWorkFactory.Create())
            {
                Restaurant model = unitOfWork.Restaurant.GetByIdWithDishes(id);
                RestaurantInfoModel restaurantInfoModel = Mapper.Map<RestaurantInfoModel>(model);
                return restaurantInfoModel;
            }
        }
    }
}
