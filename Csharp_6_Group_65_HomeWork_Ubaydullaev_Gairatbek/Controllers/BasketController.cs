﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Models;
using Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Services.Contracts;
using HomeWork_65.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Controllers
{
    public class BasketController : Controller
    {
        private readonly IBasketService _basketService;
        private UserManager<User> _userManager;

        public BasketController(IBasketService basketService, UserManager<User> userManager)
        {
            if (basketService == null)
                throw new ArgumentNullException(nameof(basketService));
            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));
            _basketService = basketService;
            _userManager = userManager;
        }

        [Authorize]
        public async Task<IActionResult> Index()
        {           
            try
            {
                User currentUser = await _userManager.GetUserAsync(User);
                List<BasketModel> model = _basketService.ShowBaskets(currentUser.Id);
                return View(model);
            }
            catch(ArgumentNullException )
            {
                return BadRequest();
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
            
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddToBasket(int id)
        {           
            try
            {
                User currentUser = await _userManager.GetUserAsync(User);
                _basketService.AddToBasket(id, currentUser.Id);
                return RedirectToAction("Index", "Restaurant");
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }
    }
}