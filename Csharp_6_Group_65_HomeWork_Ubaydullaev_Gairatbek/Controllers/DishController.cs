﻿using System;
using Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Models;
using Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Services.Contracts;
using HomeWork_65.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Controllers
{
    public class DishController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IDishService _dishService;

        public DishController(UserManager<User> userManager, IDishService dishService)
        {
            if(dishService == null)
                throw new ArgumentNullException(nameof(_dishService));
            if(userManager == null)
                throw new ArgumentNullException(nameof(_userManager));
            _userManager = userManager;
            _dishService = dishService;
        }

        public IActionResult Index()
        {
            try
            {
                var AllDishes = _dishService.GetAllDishes();
                return View(AllDishes);
            }
            catch (ArgumentNullException e)
            {
                throw new ArgumentNullException(nameof(e.Message));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize]
        [HttpGet]
        public IActionResult Create()
        {            
            try
            {
                var model = _dishService.GetDishCreateModel();
                return View(model);
            }
            catch (ArgumentNullException e)
            {
                throw new ArgumentNullException(nameof(e.Message));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize]
        [HttpPost]
        public IActionResult CreateDish(DishCreateModel model)
        {
            try
            {
                _dishService.Create(model);
                return RedirectToAction("Index");
            }
            catch (ArgumentNullException e)
            {
                throw new ArgumentNullException(nameof(e.Message));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}