﻿using Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Models;
using Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Services.Contracts;
using HomeWork_65.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Controllers
{
    public class RestaurantController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IRestaurantService _restaurantService;

        public RestaurantController(UserManager<User> userManager, IRestaurantService restaurantService)
        {
            if(restaurantService == null)
                throw new ArgumentNullException(nameof(_restaurantService));
            if(userManager == null)
                throw new ArgumentNullException(nameof(_userManager));
            _userManager = userManager;
            _restaurantService = restaurantService;
        }

        public IActionResult Index()
        {
            try
            {
                var AllRestaurants = _restaurantService.GetAllRestaurant();
                return View(AllRestaurants);
            }
            catch(ArgumentNullException e)
            {
                throw new ArgumentNullException(nameof(e.Message));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize]
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public IActionResult CreateRestaurant(RestaurantCreateModel model)
        {
            try
            {
                _restaurantService.Create(model);

                return RedirectToAction("Index");
            }
            catch (ArgumentNullException e)
            {
                throw new ArgumentNullException(nameof(e.Message));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new Exception(nameof(e.Message));
            }
            
        }

        public IActionResult ShowRestaurant(int Id)
        {

            try
            {
                RestaurantInfoModel model = _restaurantService.GetRestaurantById(Id);
                return View(model);
            }
            catch (ArgumentNullException e)
            {
                throw new ArgumentNullException(nameof(e.Message));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new Exception(nameof(e.Message));
            }
            
        }
    }
}