﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Models
{
    public class BasketModel
    {
        public int Id { get; set; }
        public decimal Sum { get; set; }
        public int DishCount { get; set; }
        public string DishName { get; set; }
        public string Restaurant { get; set; }
        public string Username { get; set; }
    }
}
