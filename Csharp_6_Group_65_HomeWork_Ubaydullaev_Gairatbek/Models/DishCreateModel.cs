﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.ComponentModel.DataAnnotations;

namespace Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Models
{
    public class DishCreateModel
    {
        [Required]
        [Display(Name ="Название")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Цена")]
        [Range(0, Double.PositiveInfinity, ErrorMessage = "Цена не может быть отрицательной")]
        public decimal Price { get; set; }

        [Required]
        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Ресторан")]
        public int RestaurantId { get; set; }
        public SelectList RestorantsSelect { get; set; }
    }
}
