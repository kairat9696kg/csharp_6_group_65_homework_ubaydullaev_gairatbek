﻿using System.Collections.Generic;

namespace Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Models
{
    public class RestaurantModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
