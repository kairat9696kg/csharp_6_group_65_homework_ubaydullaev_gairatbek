﻿using AutoMapper;
using Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek.Models;
using HomeWork_65.Entities;

namespace Csharp_6_Group_65_HomeWork_Ubaydullaev_Gairatbek
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateRestaurantToRestaurantModel();
            CreateDishToDishModel();
            CreateRestaurantCreateModelToRestaurant();
            CreateDishCreateModelToDish();
            CreateRestaurantToRestaurantInfoModel();
            CreateBasketToBasketModel();
        }

        private void CreateBasketToBasketModel()
        {
            CreateMap<Basket, BasketModel>()
                .ForMember(target => target.DishName,
                    src => src.MapFrom(p => p.Dish.Name))
                .ForMember(target => target.Restaurant,
                    src => src.MapFrom(p => p.Restaurant.Name))
                .ForMember(target => target.Username,
                    src => src.MapFrom(p => p.User.UserName));

        }
        private void CreateDishCreateModelToDish()
        {
            CreateMap<DishCreateModel, Dish>();
        }
        private void CreateRestaurantToRestaurantModel()
        {
            CreateMap<Restaurant, RestaurantModel>(); 
        }


        private void CreateRestaurantToRestaurantInfoModel()
        {
            CreateMap<Restaurant, RestaurantInfoModel>()
                .ForMember(target => target.Dishes,
                    src => src.MapFrom(p => p.Dishes));
        }
        private void CreateDishToDishModel()
        {
            CreateMap<Dish, DishModel>()
                .ForMember(target => target.Restaurant,
                    src => src.MapFrom(p => p.Restaurant.Name));
        }
        private void CreateRestaurantCreateModelToRestaurant()
        {
            CreateMap<RestaurantCreateModel, Restaurant>();
        }
    }
}
